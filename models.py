from app import db


class Todos(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    todo = db.Column(db.String(), unique=False, nullable=False)
    desc = db.Column(db.Text(), unique=False, nullable=True)
    start_date = db.Column(db.Date(), unique=False, nullable=False)
    deadline = db.Column(db.Date(), unique=False, nullable=False)
    time = db.Column(db.Time(), unique=False, nullable=True)
    done = db.Column(db.Boolean(), unique=False, nullable=False, default=False)
