from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired


class AddTask(FlaskForm):
    todo = StringField('Task Name', validators=[DataRequired()])
    desc = TextAreaField('Task Description')
    start_date = StringField('Start Date')
    deadline = StringField('Deadline')
    time = StringField('Time')
    submit = SubmitField('Add Task')


class EditTask(AddTask):
    submit = SubmitField('Edit Task')