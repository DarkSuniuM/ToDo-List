from flask import Flask, render_template, request, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import or_
from datetime import datetime as dt

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'This is so fuckin secret!:|'
db = SQLAlchemy(app)
migrate = Migrate(app=app, db=db)

from models import Todos
from forms import AddTask, EditTask


@app.route('/')
def all_tasks():
    tasks = db.session.query(Todos).all()
    return render_template('todos_list.html', title="All Tasks", tasks=tasks)


@app.route('/today')
def today_tasks():
    tasks = db.session.query(Todos).filter_by(deadline=dt.date(dt.today())).all()
    return render_template('todos_list.html', title="Today Tasks", tasks=tasks)


@app.route('/search/<string:keyword>')
def search(keyword):
    tasks = db.session.query(Todos).filter(
        or_(Todos.todo.ilike(f"%{keyword}%"), Todos.desc.ilike(f"%{keyword}%"))).all()
    return render_template('todos_list.html', title=f"Search for {keyword} in Tasks", tasks=tasks)


@app.route('/add', methods=['GET', 'POST'])
def add_task():
    form = AddTask()
    if request.method == 'POST':
        if form.validate_on_submit():
            new_task = Todos(todo=form.todo.data,
                             desc=form.desc.data if form.desc.data else None,
                             start_date=dt.strptime(form.start_date.data, '%Y-%m-%d') if form.start_date.data else None,
                             deadline=dt.strptime(form.deadline.data, '%Y-%m-%d') if form.deadline.data else None,
                             time=dt.strptime(form.time.data, '%H:%M').time() if form.time.data else None)
            db.session.add(new_task)
            try:
                db.session.commit()
                flash('Success')
                return redirect(url_for('all_tasks'))
            except Exception as e:
                db.session.rollback()
                print(str(e))
                flash(str(e) + '\n Failed')
                return redirect(url_for('add_task'))
    return render_template('add_task.html', title='Add a New Task', form=form)


@app.route('/edit/<int:task_id>', methods=['GET', 'POST'])
def edit_task(task_id):
    chosen_task = db.session.query(Todos).filter_by(id=task_id).first()
    if chosen_task:
        form = EditTask(todo=chosen_task.todo,
                        desc=chosen_task.desc,
                        start_date=chosen_task.start_date,
                        deadline=chosen_task.deadline,
                        time=chosen_task.time)
        if request.method == 'POST':
            if form.validate_on_submit():
                chosen_task.todo = form.todo.data
                chosen_task.desc = form.desc.data if form.desc.data else None
                chosen_task.start_date = dt.strptime(form.start_date.data, '%Y-%m-%d') if form.start_date.data else None
                chosen_task.deadline = dt.strptime(form.deadline.data, '%Y-%m-%d') if form.deadline.data else None
                chosen_task.time = dt.strptime(form.time.data, '%H:%M').time() if form.time.data else None
                try:
                    db.session.commit()
                    flash(f'Task #{task_id} Updated')
                    return redirect(url_for('all_tasks'))
                except Exception as e:
                    db.session.rollback()
                    flash(str(e) + '\n Failed')
                    print(str(e))
                    return redirect(url_for('edit_task', task_id=chosen_task.id))
        return render_template('edit_task.html', form=form, title=f"Edit Task #{chosen_task.id}")
    return "404", 404


@app.route('/remove/<int:task_id>')
def remove_task(task_id):
    chosen_task = db.session.query(Todos).filter_by(id=task_id).first()
    if chosen_task:
        db.session.query(Todos).filter_by(id=task_id).delete()
        try:
            db.session.commit()
            flash('Task Removed Successfully')
            return redirect(url_for('all_tasks'))
        except Exception as e:
            db.session.rollback()
            flash(str(e))
            return redirect(url_for('all_tasks'))
    return "404", 404


@app.route('/done/<int:task_id>')
def done_task(task_id):
    chosen_task = db.session.query(Todos).filter_by(id=task_id).first()
    if chosen_task:
        chosen_task.done = not chosen_task.done
        try:
            db.session.commit()
            flash('Task status changed Successfully')
            return redirect(url_for('all_tasks'))
        except Exception as e:
            db.session.rollback()
            flash(str(e))
            return redirect(url_for('all_tasks'))
    return "404", 404


@app.route('/undone/<int:task_id>')
def undone_task(task_id):
    chosen_task = db.session.query(Todos).filter_by(id=task_id).first()
    if chosen_task:
        chosen_task.done = not chosen_task.done
        try:
            db.session.commit()
            flash('Task status changed Successfully')
            return redirect(url_for('all_tasks'))
        except Exception as e:
            db.session.rollback()
            flash(str(e))
            return redirect(url_for('all_tasks'))
    return "404", 404


if __name__ == '__main__':
    app.run(port=6666)
