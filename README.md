A super lazy and simple "ToDo-List" application, by Flask!

# How to use

1. `git clone https://gitlab.com/DarkSuniuM/ToDo-List.git && cd ToDo-List`
2. `pip install -r requirments.txt`
3. `export FLASK_APP=app.py`
4. `flask run`

It should run on `http://127.0.0.1:5000`


# ToDo

- Add Filters
- Add Search

_[It's funny that I have a ToDo-List for a ToDo-List Application ]_


# Special Thanks

- @NarimanMov (for helping me with JavaScript Parts on commit "bd7b0ae02a1c9727b18e82d5eea7037252fa24bf")